#!/bin/bash -e

global_pkg_db=`stack path --global-pkg-db`
bytestring_conf=`ls $global_pkg_db | grep bytestring`
lib_dir=`cat $global_pkg_db/$bytestring_conf | grep library-dirs | awk '{ print $2 }'`
bytestring_lib=$lib_dir/`ls $lib_dir | grep "\.a$" | grep -v "_p\.a$"`

stack ghc ./src/Processing.hs

ld -r -o Processing.o ./src/Processing.o $bytestring_lib

#if command -v stack; then
    #GHC="stack ghc --package bytestring -- "
#elif command -v ghc; then
    #GHC="ghc"
#else
    #echo "No GHC found"
    #return 1
#fi

## -c : skip linking step (would be bypassed anyway)
## -o : we redirect the output
#$GHC -c -outputdir tmp -o Processing-unlinked.o src/Processing.hs

## Linking step
## -o Processing.o : we redirect the output to our final object file
#ld -r -o Processing.o Processing-unlinked.o $bytestring_lib

#mv Processing-unlinked.o tmp
