all: client processing server

client: src/Client.hs
	./build-client.sh

processing: src/Processing.hs
	./build-processing.sh

server: src/Server.hs
	./build-server.sh

clean: FORCE
	./clean.sh
	rm -f ./src/*.hi ./src/*.o

FORCE:
