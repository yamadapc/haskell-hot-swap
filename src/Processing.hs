{-# LANGUAGE OverloadedStrings #-}

module Processing(f) where

import qualified Data.ByteString as B

-- f :: B.ByteString -> B.ByteString
-- f bs = "You are a great '" `B.append` bs `B.append` "'"

f :: B.ByteString -> B.ByteString
f bs = "You are an awesome '" `B.append` bs `B.append` "'"
